module GoWeatherApp

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/labstack/echo/v4 v4.1.16
)
